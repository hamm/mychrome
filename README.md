
# C# Chrome内核浏览器封装接口文档

***

<img src="https://svg.hamm.cn/gitee.svg?type=star&user=hamm&project=mychrome">
<img src="https://svg.hamm.cn/gitee.svg?type=fork&user=hamm&project=mychrome">
<img src="https://svg.hamm.cn/gitee.svg?type=watch&user=hamm&project=mychrome">
<img src="https://svg.hamm.cn/gitee.svg?type=commit&user=hamm&project=mychrome">

 
### 窗口相关
```
chrome.helloWorld("Hello World!");//测试方法弹窗
chrome.windowTitle("Hello World!");//设置窗口标题
chrome.windowMin();//应用最小化
chrome.windowMax();//应用最大化
```

### 存储相关

```
chrome.removeAllCatch();//删除所有缓存
chrome.removeCatch("key");//删除缓存
chrome.setCatch("key", object);//设置缓存
chrome.getCatch("key");//获取缓存
chrome.setAppConfig("key","value");//设置应用配置
chrome.getAppConfig("key");//获取应用配置
```
### 语音相关

```
chrome.getVoiceList();//获取语音列表
chrome.playVoice("nihao","voice name");//播放语音
```

### 系统相关
```
chrome.showScreenKeyboard();//弹出屏幕键盘
chrome.getSystemVersion();//获取系统版本
chrome.windowClose();//关闭当前应用
```


### 配置文件

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration> 
  <startup> 
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2"/> 
  </startup>
  <appSettings>
    <!--首页地址-->
    <add key="homepage" value="local://Hamm/index.html"/>
    <!--是否允许F12打开调试工具-->
    <add key="devtools" value="true"/>
    <!--是否全屏最大化-->
    <add key="fullscreen" value="yes"/>
    <!--最大化按钮是否显示-->
    <add key="btn_max" value="yes"/>
    <!--最小化按钮是否显示-->
    <add key="btn_min" value="yes"/>
    <!--是否显示操作按钮-->
    <add key="btns" value="yes"/>
    <!--是否置顶-->
    <add key="topmost" value="no"/>
    <!--边框样式 resizable fixed no none -->
    <add key="border" value="resizable"/>
    <!--是否显示到任务栏-->
    <add key="showintaskbar" value="yes"/>
    <!--标题栏是否显示图标-->
    <add key="showicon" value="yes"/>
    <!--窗体标题-->
    <add key="title" value="HelloWorld"/>
  </appSettings>
</configuration>

```