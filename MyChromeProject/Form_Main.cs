﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Speech.Recognition;//引用系统的Speech的识别
using System.Speech.Synthesis; //引用语音合成
using System.IO;
using System.Threading;
using System.Security.Cryptography;

namespace MyChromeProject
{
        public partial class Form_Main : Form
        {
                DriveInfo Tdriver = null;
                public ChromeConfigHelper cch = new ChromeConfigHelper();
                public string chromeUrl = "";
                protected string publicKey = "";
                public Form_Main()
                {
                        publicKey = Encoding.Default.GetString(MyChromeProject.Properties.Resources._public);
                        InitializeComponent();
                        this.Hide();
                        InitConfigs();
                        InitChromiumWebBrowser();
                        this.Show();
                }

                /// <param name="iFrequency">声音频率（从37Hz到32767Hz）。在windows95中忽略</param> 
                /// <param name="iDuration">声音的持续时间，以毫秒为单位。</param> 
                [DllImport("Kernel32.dll")] //引入命名空间 using System.Runtime.InteropServices; 
                public static extern bool Beep(int frequency, int duration);

                protected override void WndProc(ref Message m)
                {
                        if (m.Msg == 0x0219)//WM_DEVICECHANGE
                        {
                                switch (m.WParam.ToInt32())
                                {
                                        case 0x8000://DBT_DEVICEARRIVAL
                                                Beep(1000, 200);
                                                Beep(2000, 200);
                                                string[] dirs = Environment.GetLogicalDrives(); //取得所有的盘符 
                                                foreach (string dir in dirs)
                                                {
                                                        Tdriver = new DriveInfo(dir);
                                                        if (Tdriver.DriveType == DriveType.Removable)
                                                        {
                                                                while (Tdriver.IsReady == false)
                                                                {
                                                                        Thread.Sleep(500);
                                                                }
                                                                try
                                                                {
                                                                        string path_username = Tdriver.RootDirectory.ToString() + @"__Hamm.cn__Authroize__\.user.name";
                                                                        string username = File.ReadAllText(path_username);
                                                                        string path_password = Tdriver.RootDirectory.ToString() + @"__Hamm.cn__Authroize__\.pass.word";
                                                                        string password = File.ReadAllText(path_password);

                                                                        Console.WriteLine(username);
                                                                        Console.WriteLine(password);

                                                                        username = RsaHelper.DecryptWithPublicKey(publicKey, username);
                                                                        password = RsaHelper.DecryptWithPublicKey(publicKey, password);

                                                                        Console.WriteLine(username);
                                                                        Console.WriteLine(password);

                                                                        String script = "var myEvent = new CustomEvent('chrome', {detail:{type:'usb2login',data:{username:'" + username+ "',password:'" + password+ "'}}}); window.dispatchEvent(myEvent);";
                                                                        Console.WriteLine(script);
                                                                        chromeBrowser.ExecuteScriptAsync(script);

                                                                }
                                                                catch(Exception err)
                                                                {
                                                                        Console.WriteLine(err.Message);
                                                                }
                                                        }
                                                }
                                                break;
                                        case 0x8004://DBT_DEVICEREMOVECOMPLETE
                                                Beep(2000, 200);
                                                Beep(1000, 200);
                                                break;
                                }
                        }
                        base.WndProc(ref m);
                }
                private static RSA Init(String keypair)
                {
                        var rsa = new RSACryptoServiceProvider();
                        rsa.ImportCspBlob(Convert.FromBase64String(keypair));
                        return rsa;
                }
                public void InitConfigs()
                {
                        string fullscreen = cch.getAppConfig("fullscreen");
                        switch (fullscreen)
                        {
                                case "show":
                                case "yes":
                                case "true":
                                        this.WindowState = FormWindowState.Maximized;
                                        break;
                                case "hide":
                                case "no":
                                case "false":
                                default:
                                        this.WindowState = FormWindowState.Normal;
                                        break;
                        }
                        string btn_max = cch.getAppConfig("btn_max");
                        switch (btn_max)
                        {
                                case "hide":
                                case "no":
                                case "false":
                                        this.MaximizeBox = false;
                                        break;
                                case "show":
                                case "yes":
                                case "true":
                                default:
                                        this.MaximizeBox = true;
                                        break;
                        }
                        string btn_min = cch.getAppConfig("btn_min");
                        switch (btn_max)
                        {
                                case "hide":
                                case "no":
                                case "false":
                                        this.MinimizeBox = false;
                                        break;
                                case "show":
                                case "yes":
                                case "true":
                                default:
                                        this.MinimizeBox = true;
                                        break;
                        }
                        string btns = cch.getAppConfig("btns");
                        switch (btns)
                        {
                                case "show":
                                case "yes":
                                case "true":
                                        this.ControlBox = true;
                                        break;
                                case "hide":
                                case "no":
                                case "false":
                                default:
                                        this.ControlBox = false;
                                        break;
                        }
                        string topmost = cch.getAppConfig("topmost");
                        switch (topmost)
                        {
                                case "show":
                                case "yes":
                                case "true":
                                        this.TopMost = true;
                                        break;
                                case "hide":
                                case "no":
                                case "false":
                                default:
                                        this.TopMost = false;
                                        break;
                        }
                        string border = cch.getAppConfig("border");
                        switch (border)
                        {
                                case "hide":
                                case "no":
                                        this.FormBorderStyle = FormBorderStyle.None;
                                        break;
                                case "sizable":
                                        this.FormBorderStyle = FormBorderStyle.Sizable;
                                        break;
                                case "fixed":
                                        this.FormBorderStyle = FormBorderStyle.Fixed3D;
                                        break;
                                default:
                                        this.FormBorderStyle = FormBorderStyle.Sizable;
                                        break;
                        }
                        string showintaskbar = cch.getAppConfig("showintaskbar");
                        switch (showintaskbar)
                        {
                                case "hide":
                                case "no":
                                case "false":
                                        this.ShowInTaskbar = false;
                                        break;
                                case "show":
                                case "yes":
                                case "true":
                                default:
                                        this.ShowInTaskbar = true;
                                        break;
                        }
                        string showicon = cch.getAppConfig("showicon");
                        switch (showicon)
                        {
                                case "hide":
                                case "no":
                                case "false":
                                        this.ShowIcon = false;
                                        break;
                                case "show":
                                case "yes":
                                case "true":
                                default:
                                        this.ShowIcon = true;
                                        break;
                        }
                        string title = cch.getAppConfig("title");
                        if (title == "")
                        {
                                this.Text = "Hamm.cn";
                        }
                        else
                        {
                                this.Text = title;
                        }
                }
                public ChromiumWebBrowser chromeBrowser;
                /// <summary>
                /// 初始化浏览器
                /// </summary>
                public void InitChromiumWebBrowser()
                {
                        try
                        {
                                ChromeConfigHelper configHelper = new ChromeConfigHelper();
                                string configUrl = configHelper.getAppConfig("homepage");
                                if (configUrl != null)
                                {
                                        this.chromeUrl = configUrl;
                                }
                        }
                        catch (Exception err)
                        {
                                Console.WriteLine(err.Message);
                        }
                        CefSettings settings = new CefSettings();
                        settings.RegisterScheme(new CefCustomScheme
                        {
                                SchemeName = ChromeSchemeFactory.mySchemeName,
                                SchemeHandlerFactory = new ChromeSchemeFactory()
                        });
                        //缓存路径
                        settings.CachePath = "/BrowserCache";
                        //浏览器引擎的语言
                        settings.AcceptLanguageList = "zh-CN,zh;q=0.8";
                        //日志文件
                        settings.LogFile = "/LogData";
                        settings.PersistSessionCookies = true;
                        settings.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
                        Cef.Initialize(settings);
                        chromeBrowser = new ChromiumWebBrowser(this.chromeUrl);
                        chromeBrowser.Dock = DockStyle.Fill;
                        this.Controls.Add(chromeBrowser);
                        BrowserSettings browserSettings = new BrowserSettings();
                        browserSettings.FileAccessFromFileUrls = CefState.Enabled;
                        browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
                        browserSettings.WebSecurity = CefState.Enabled;

                        chromeBrowser.BrowserSettings = browserSettings;
                        CefSharpSettings.LegacyJavascriptBindingEnabled = true;
                        chromeBrowser.RegisterJsObject("chrome", new ChromeJavaScriptHelper(this));


                        chromeBrowser.MenuHandler = new ChromeMenuHandler();
                        chromeBrowser.KeyboardHandler = new ChromeKeyboradHandler();
                        chromeBrowser.IsBrowserInitializedChanged += OnIsBrowserInitializedChanged;
                        chromeBrowser.FrameLoadEnd += OnFrameLoadEnd;
                }

        private void OnIsBrowserInitializedChanged(object sender, EventArgs args)
        {
            //chrome初始化完成
        }

        private void OnFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
                {
                        //页面加载完成
                        String script = "var myEvent = new CustomEvent('chrome', {detail:{'type':'ready'}}); window.dispatchEvent(myEvent);";
                        chromeBrowser.ExecuteScriptAsync(script);
                }

                private void Form_Main_FormClosing(object sender, FormClosingEventArgs e)
                {
                        //关闭前需要将chrome释放
                        this.Hide();
                        Cef.Shutdown();
                }

                private void Form_Main_Load(object sender, EventArgs e)
                {

                }
        }
}
