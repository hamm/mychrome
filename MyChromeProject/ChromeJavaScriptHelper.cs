using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyChromeProject
{
    public class ChromeJavaScriptHelper
    {
        private Dictionary<string, object> data = new Dictionary<string, object>();

        private delegate void windowControllerDelegate(string param = null);
        private bool windowController(windowControllerDelegate wc, string param = null)
        {
            form_main.Invoke(wc, param);
            return true;
        }
        private Form_Main form_main;
        public ChromeJavaScriptHelper(Form_Main fm)
        {
            form_main = fm;
        }
        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <returns></returns>
        public bool windowClose()
        {
            windowController(windowClose_task);
            return true;
        }
        private void windowClose_task(string param)
        {
            form_main.Close();
        }
        /// <summary>
        /// 设置窗口标题
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public bool windowTitle(string title = "Hamm.cn")
        {
            windowController(windowTitle_task, title);
            return true;
        }
        private void windowTitle_task(string param)
        {
            form_main.Text = param;
        }
        /// <summary>
        /// 窗口最小化
        /// </summary>
        /// <returns></returns>
        public bool windowMin()
        {
            windowController(windowMin_task);
            return true;
        }
        private void windowMin_task(string param)
        {
            form_main.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// 窗口最大化
        /// </summary>
        /// <returns></returns>
        public bool windowMax()
        {
            windowController(windowMax_task);
            return true;
        }
        private void windowMax_task(string param)
        {
            form_main.WindowState = FormWindowState.Maximized;
        }
        /// <summary>
        /// 删除全部缓存
        /// </summary>
        /// <returns></returns>
        public bool removeAllCatch()
        {
            this.data = null;
            this.data = new Dictionary<string, object>();
            return true;
        }
        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool removeCatch(string key)
        {
            this.data.Remove(key);
            return true;
        }
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool setCatch(string key, object value)
        {
            this.data.Remove(key);
            this.data.Add(key, value);
            return true;
        }
        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object getCatch(string key)
        {
            object tmp = null;
            this.data.TryGetValue(key, out tmp);
            if (tmp == null)
            {
                return null;
            }
            else
            {
                return tmp;
            }
        }
        /// <summary>
        /// 获取当前系统支持的语音列表
        /// </summary>
        /// <returns></returns>
        public string[] getVoiceList()
        {
            return new ChromeVoiceHelper().getVoiceList();
        }
        /// <summary>
        /// 测试方法 弹窗
        /// </summary>
        /// <param name="message"></param>
        public void helloWorld(String message)
        {
            MessageBox.Show(message);
        }
        /// <summary>
        /// 播放语音
        /// </summary>
        /// <param name="message"></param>
        public void playVoice(string message = "语音模块初始化成功", string who = "Microsoft Huihui Desktop")
        {
            ChromeVoiceHelper voice = new ChromeVoiceHelper();
            voice.playVoice(message, who);
        }
        /// <summary>
        /// 设置APP配置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool setAppConfig(string key, string value)
        {
            ChromeConfigHelper configHelper = new ChromeConfigHelper();
            configHelper.setAppConfig(key, value);
            return true;
        }
        /// <summary>
        /// 获取APP配置
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string getAppConfig(string key)
        {
            ChromeConfigHelper configHelper = new ChromeConfigHelper();
            return configHelper.getAppConfig(key);
        }
        /// <summary>
        /// 显示屏幕键盘
        /// </summary>
        public void showScreenKeyboard()
        {
            Process.Start("osk.exe");
        }
        /// <summary>
        /// 获取系统版本字符串
        /// </summary>
        /// <returns></returns>
        public string getSystemVersion()
        {
            return System.Environment.OSVersion.VersionString;
        }
        /// <summary>
        /// 退出应用程序
        /// </summary>
        public void exitApp()
        {
            Environment.Exit(0);
        }
    }
}
