﻿using CefSharp;

namespace MyChromeProject
{
        internal class ChromeSchemeFactory : ISchemeHandlerFactory
        {
                public const string mySchemeName = "local";
                public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
                {
                        if (schemeName == mySchemeName)
                        {
                                return new ChromeSchemeHandler();
                        }
                        else
                        {
                                var mimeType = ResourceHandler.GetMimeType(".html");
                                return ResourceHandler.FromFilePath("aaa.html", mimeType);
                        }
                }
        }
}