﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyChromeProject
{
        public class ChromeConfigHelper
        {
                ///<summary> 
                ///返回*.exe.config文件中appSettings配置节的value项  
                ///</summary> 
                ///<param name="strKey"></param> 
                ///<returns></returns> 
                public string getAppConfig(string key)
                {
                        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        if (config.AppSettings.Settings[key] == null)
                                return null;
                        else
                                return config.AppSettings.Settings[key].Value;
                }
                public bool setAppConfig(string key, string value)
                {
                        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        if (config.AppSettings.Settings[key] == null)
                        {
                                config.AppSettings.Settings.Add(key, value);
                        }
                        else
                        {
                                config.AppSettings.Settings[key].Value = value;
                        }
                        config.Save(ConfigurationSaveMode.Modified);
                        ConfigurationManager.RefreshSection("appSettings");
                        //重新加载新的配置文件   
                        return true;
                }
        }
}
