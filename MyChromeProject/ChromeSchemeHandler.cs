﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.Callback;

namespace MyChromeProject
{
        internal class ChromeSchemeHandler : IResourceHandler
        {
                //内存流
                private MemoryStream ms;
                //mimeType,后面输出的时候要用
                private string mimeType;
                public ChromeSchemeHandler()
                {
                }

                public void Cancel()
                {
                }

                public bool CanGetCookie(Cookie cookie)
                {
                        return false;
                }

                public bool CanSetCookie(Cookie cookie)
                {
                        return false;
                }

                public void Dispose()
                {
                        if (ms != null)
                        {
                                ms.Close();
                                ms.Dispose();
                                ms = null;
                                mimeType = null;
                        }
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                }

                public void GetResponseHeaders(IResponse response, out long responseLength, out string redirectUrl)
                {
                        redirectUrl = null;
                        response.MimeType = mimeType;
                        if (ms != null)
                        {
                                responseLength = ms.Length;
                                response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                                response.StatusText = "OK";
                        }
                        else
                        {
                                responseLength = 0;
                                response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
                                response.StatusText = "Not Found";
                        }
                }

        public bool Open(IRequest request, out bool handleRequest, ICallback callback)
        {
            throw new NotImplementedException();
        }

        public bool ProcessRequest(IRequest request, ICallback callback)
                {
                        var uri = new Uri(request.Url);
                        var type = uri.Authority;
                        var resName = System.Environment.CurrentDirectory+ "/page"+ uri.AbsolutePath;
                        Console.WriteLine(resName);
                        switch (type)
                        {
                                case "images":
                                        break;
                                default:
                                        if (File.Exists(resName))
                                        {
                                                Task.Run(() =>
                                                {
                                                        using (callback)
                                                        {
                                                                ms = new MemoryStream(File.ReadAllBytes(resName));
                                                                string ext = Path.GetExtension(resName);
                                                                mimeType = ResourceHandler.GetMimeType(ext);
                                                                callback.Continue();
                                                        }
                                                });
                                                return true;
                                        }
                                        else {
                                                Console.WriteLine("File not found!");
                                        }
                                        return false;
                        }
                        return false;
                }

        public bool Read(Stream dataOut, out int bytesRead, IResourceReadCallback callback)
        {
            throw new NotImplementedException();
        }

        public bool ReadResponse(Stream dataOut, out int bytesRead, ICallback callback)
                {
                        callback.Dispose();
                        if (ms == null)
                        {
                                bytesRead = 0;
                                return false;
                        }
                        var buffer = new byte[dataOut.Length];
                        bytesRead = ms.Read(buffer, 0, buffer.Length);
                        dataOut.Write(buffer, 0, buffer.Length);
                        return bytesRead > 0;
                }

        public bool Skip(long bytesToSkip, out long bytesSkipped, IResourceSkipCallback callback)
        {
            throw new NotImplementedException();
        }
    }
}