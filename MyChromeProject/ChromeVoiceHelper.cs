﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyChromeProject
{

        public class ChromeVoiceHelper
        {
                public ChromeVoiceHelper()
                {
                        speech = new SpeechSynthesizer();
                }
                public string whoToSpeak = "";
                public string whatToSpeak = "语音模块初始化成功";
                /// <summary>
                /// 语音朗读
                /// </summary>
                /// <param name="message"></param>
                public void playVoice(string message= "语音模块初始化成功", string who= "Microsoft Huihui Desktop")
                {
                        whoToSpeak = who;
                        whatToSpeak = message;
                        new Thread(Speak).Start();
                }
                public string[] getVoiceList()
                {
                        List<string> list = new List<string>();
                        foreach (InstalledVoice voice in speech.GetInstalledVoices())
                        {
                                list.Add(voice.VoiceInfo.Name);
                        }
                        return list.ToArray();
                }
                private SpeechSynthesizer speech;
                private void Speak()
                {
                        speech.SelectVoice(whoToSpeak);
                        speech.Volume = 100;
                        speech.Rate = 2;
                        speech.SpeakAsync(whatToSpeak);//语音阅读方法
                        speech.SpeakCompleted += speech_SpeakCompleted;//绑定事件
                }
                private void speech_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
                {
                }


        }
}
