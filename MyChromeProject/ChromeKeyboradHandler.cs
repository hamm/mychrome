﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyChromeProject
{
        class ChromeKeyboradHandler : IKeyboardHandler
        {
                public bool OnKeyEvent(IWebBrowser chromiumWebBrowser, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey)
                {
                        return false;
                }

                public bool OnPreKeyEvent(IWebBrowser chromiumWebBrowser, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey, ref bool isKeyboardShortcut)
                {
                        ChromeConfigHelper cch = new ChromeConfigHelper();
                        if (type == KeyType.RawKeyDown)
                        {
                                switch (windowsKeyCode)
                                {
                                        case 123:
                                                //F12
                                                string devtools = cch.getAppConfig("devtools");
                                                if (devtools == "true") { 
                                                        chromiumWebBrowser.ShowDevTools();
                                                }
                                                break;
                                        default:
                                                return false;
                                }
                                return true;
                        }
                        else
                        {
                                return false;
                        }
                }
        }
}
